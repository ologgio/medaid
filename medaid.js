// Set to true to enable debug info printed to the console
_MEDAID_DEBUG=true;
_MEDAID_VERSION = 0.12

//########################################################################
//########################################################################
//------------------------- INITIALIZATION -------------------------------
//########################################################################
//########################################################################
//

// Medicine type constants
// Main object (will be abbreviated $ma) 
medaid = { 
           DAILY : "Daily",
           WEEKLY: "Weekly",
           ASNEEDED: "As needed",

           console: {  assert: function(){},  
                        log: function(){},  
                        warn: function(){},  
                        error: function(){},  
                        debug: function(){},  
                        dir: function(){},  
                        info: function(){}
                     },

            // The object contained in the array exists only for
            // documentation purposes so that the properties 
            // may be known easily.
				meds: [{ name: "Keppra",			   // Medicine name
							desc: "Levetiracetam",	   // Description
							type: "Daily",			      // Daily, Weekly, or As needed
							doses:[{
								time: new Date(2012,1,1,9,00),
								count: 3 
							},
							{ 
							  	time: new Date(2012,1,1,21,00),
								count: 3 
							}								// time/date for dose and 
															// number of doses
							],
							dosesLeft: 30,          // Pills or inhalations left
							dosesLeftDate: new Date(2012,1,1,9,0), // Date for doses left
							daysBeforeRunoutAlarm: 12, // Alarm on if runout date
		                                // is within this number of days
							onfile: true,              // True if prescription on file
		                                // at Mayo
							refillOrdered: true,       // True if refill has been
		                                // ordered
                     refillsLeft: 8,
                     pharmacy: "Mayo pharmacy",
                     pharmacyPhone: "(800) 445-6326",
                     rxNumber: 9783982,
							_dosesPerPeriod: 6,        // Total doses per period
							_runoutDate: new Date(2012,1,20,21,00),
							_refillDate: new Date(2012,2,22,21,00),
							_daysLeft: 20,             // Number of days left with
		                                // at current dosage
                     _daysUntilRefill: 20,
							_alarm: true               // Alarm to refill
                  }],

            //Set to true when all calculations are finished
            ready: false,
            version: _MEDAID_VERSION
};


// Abbreviated name
$ma=medaid;
$ma.tout = [];

//********************************************************************
// Purpose:       Calculate all the necessary values for all the 
//                medicines
// Parameters:    none
// Description:   This function calls updateMed() for each medicine
//******************************************************************** 
$ma.updateValues = function(callback)
{
   var i=0;
   for(i=0; i < $ma.meds.length; i++)
   {
      $ma.updateMed($ma.meds[i]);
   }
	callback();
}

//********************************************************************
// Purpose:       Calculate all the necessary values for the specified
//                medication
// Parameters:    the medication object to update 
// Description:   This function updates  _dosesPerPeriod,
//                _runoutDate, _refillDate, _daysLeft, and _alarm
//******************************************************************** 
$ma.updateMed = function(med)
{

	this.update_dosesPerPeriod(med);
	this.update_runoutDate(med);
	this.update_refillDate(med);
	//this.update_daysLeft(med);
	this.update_alarm(med);
}


//********************************************************************
// Purpose:       Calculate _dosesPerPeriod for the specified    
//                medicine
// Parameters:    Medicine to update
// Description:   This function adds the doses in the medicine
//                and calculates the total.
// Returns:       The number of doses needed per period
//                or null if there is no dosage information
//                (or medicine type is ASNEEDED)
//******************************************************************** 
$ma.update_dosesPerPeriod = function(med)
{
	// Add up the number of doses per
	// period
	var i,dose;
   sum=0;
	for(i=0; i<med.doses.length; i++)
	{
		dose = doses[i];
		sum = sum + dose.count;
	}

	// Set result to null if there is not 
	// enough information
   if ( med.doses.length <= 0 || sum === 0) 
		sum = null;
	med._dosesPerPeriod = sum;

	return med._dosesPerPeriod;
}

//********************************************************************
// Purpose:       Calculate _runoutDater for the specified    
//                medicine
// Parameters:    Medicine to update
// Description:   This function calculates the day when the medicine
//                will run out
// Returns:       The date when the medicine will run out or
//                null if daysLeft could not be calculated.
//******************************************************************** 
$ma.update_runoutDate = function(med)
{
	// runoutDate = TODAY() + _daysLeft
	//=IF(OR(H3=0,ISBLANK(E3)),"-",TODAY()+L3)

	var daysLeft = this.update_daysLeft(med);

	// Calculate runout date if daysLeft is available
	// otherwise return null
	if (daysLeft != null)
      med._runoutDate = daysLeft * 24 * 60 * 60 * 1000;
	else
		med._runoutDate = null;

	return med._runoutDate;
}

//********************************************************************
// Purpose:       Calculate _refillFate for the specified medicine    
//                
// Parameters:    Medicine to update
// Description:   This function calculates the day when the medicine
//                needs to be refilled. That is, the difference 
//                between runoutDate and daysBeforeRunoutAlarm
// Returns:       The refill date or null if the runoutDate
//                could not be calculated.
//******************************************************************** 
$ma.update_refillDate = function(med)
{
	//refillDate = runoutDate -  daysBeforeRunoutAlarm
	var runoutDate = this.update_runoutDate(med);

	// Calculate runout date if daysLeft is available
	// otherwise return null
	if (runoutDate != null)
      med._refillDate = runoutDate - daysBeforeRunoutAlarm * 24 * 60 * 60 * 1000;
	else
		med._refillDate = null;
}

//********************************************************************
// Purpose:       Calculates the _daysLeft for the specified medicine 
//                
// Parameters:    Medicine to update
// Description:   This function calculates the days left for a      
//                the medicine. It divides the doses left by   
//                the number of doses needed per period, converts
//                this number to days depending on the type of 
//                medicine (Weekly, Daily or AsNeeded) and substracts
//                this from the number of days that have passed
//                since we updated the doses left.
//
// Returns:       The days left for a medicine
//                could not be calculated.
//******************************************************************** 
$ma.update_daysLeft = function(med)
{
	// dosesLeft/dosesPerPeriod - DAYS( TODAY(),dosesLeftDate ) 
	var periodsLeft = med.dosesLeft/dosesPerPeriod;
	var daysPerPeriod=1;

	// Modify daysPerPeriod accorging to type of medicine
	switch(med.type)
	{
		case $ma.DAILY:
	      daysPerPeriod=1;
			break;
		case $ma.WEEKLY:
	      daysPerPeriod=7;
			break;
		case $ma.ASNEEDED:
	      daysPerPeriod=1;
			break;
	}
	// Milliseconds difference between today and dosesLeftDate
   var ms_diff = new Date() - med.dosesLeftDate;

	// Convert milliseconds to days
   var days_diff = ms_diff / 1000 / 60 / 60 / 24;

   //	Calculate days left of medicine
	//	a negative number means we're ahead that number of days 
	var daysLeft = periodsLeft * daysPerPeriod  - days_diff;
	med._daysLeft = daysLeft;

	return med._daysLeft;
}

$ma.update_alarm = function(med)
{
	// IF(daysLeft <= daysBeforeRunoutAlarm,"Yes","No")
   if (med._daysLeft <= daysBeforeRunoutAlarm)
	   med._alarm = true;
   else
	   med._alarm = false;	

	return med._alarm;
}

$ma.autoUpdate = function(seconds,callback)
{
	setInterval($ma.updateValues(callback), seconds * 1000);
}

$ma.tlog = function ()
{
   var i;

   for(i=0; i < arguments.length; i++)
   {
      $ma.tout.push("<li>" + arguments[i]);
   }

   $('#medaid-test').html($ma.tout.join(""));
}

// Setup console methods to empty if there is no console object
// or if _MEDAID_DEBUG is not set
if (typeof console == "undefined")
   console=medaid.console; 
if (!_MEDAID_DEBUG)
   console.debug = function(args) {};

