try 
{
   $ma.config = 
   {
      daysbefore: 0,
      daysafter: 35,
      showeaster: true
   };

   //loadConfig();

   //******** DOCUMENT  BINDINGS ********
   //************************************
   var h1Collapsibles;

   $ma.tlog("Javascript init function.");
   Zepto(function($)
   {
      $ma.tlog("Zepto initialization function.");
      //Show only the main page at first
      $('[data-role="page"]').hide().first().show();

      //Store collapsibles zepto array to use later
      h1Collapsibles = $('div[data-role="collapsible"] h1');

      //Hide section data in the beginning
      hideSection(h1Collapsibles);

      //Toggle collapsible section on click
      h1Collapsibles.on("click",function(e)
      {
         toggleSection($(this));
      })
   });

   // Hide section(s) indicated by h1el
   function hideSection(h1el)
   {
      // If caller passes a collection hide all
      // sections in the collection
      if ( $.isArray(h1el) )
         h1el.each(function(index)
         {
            hideSection(h1el);
         })

      // Hide the div after the h1 element
      // the div has the contents of that section
      // whose title is the h1 element.
      var el = h1el.next();
      if (el.is('div'))
         el.hide();
      else
         $ma.tlog('div needs to be after h1 in collapsible ' + h1el);
   }

   function toggleSection(h1el)
   {
      //Toggle the specified section
      var el = h1el.next()
      if (el.is('div'))
      {
         $ma.tlog("Toggling " + h1Collapsibles.index(h1el))
         el.toggle()
      }
      else
         $ma.tlog('div needs to be after h1 in collapsible ' + h1el)
      
      //Hide the other sections
      h1Collapsibles.each(function(index)
      {
         if ( index != h1Collapsibles.index(h1el))
            hideSection($(this))
      })
   }
}
catch(e)
{
   $ma.tlog("Exception loading medaid-ui.js: ",e);
}

//******** MAIN PAGE BINDINGS ********
//************************************
   // Hide blackberry's URL bar
//   $('#mainpage').bind("vmousemove", function(e)
//   {
//      var entered;
//      if (!entered && e.pageY <= 59) 
//      {
//         entered =true;
//         if (typeof blackberry != 'undefined')
//            $.mobile.silentScroll(59);
//      }
//      else
//      {
//         entered=false;
//      }
//   });
//
//   $('#mainpage').bind("pageinit", function()
//   {
//      // Page has now been enhanced by jQuery mobile
//
//
//      // JQuery mobile prevents links to 
//      // local anchors from working. The
//      // following code allows it for all 
//      // elements with attribute data-linkanchor 
//      // set to trueclass.
//      $('[data-linkanchor="true"]').bind('click', function(e) 
//      {
//         var offset = $( $(this).attr('href') ).get(0).offsetTop;
//         $.mobile.silentScroll(offset);
//         return false;
//      });
//
//      //********  SEARCH BAR  BINDINGS ********
//
//      // Start search if the search bar loses focus
//      $('#search-cal').change(function(e)
//      {
//         // Update main page if we have some results
//         // otherwise leave it alone
//         var stext = $('#search-cal').val();
//         searchcal(stext);
//      });
//
//      
//      //******** INITIALIZE LIST **************
//      //******** WITH TODAY AS   **************
//      //******** CENTRAL DAY     **************
//
//      // Hide the URL bar in blackberry
//      // bold 9700 (screen res. 480x360) 
//      if (typeof blackberry != 'undefined')
//         $.mobile.silentScroll(59);
//
//      updateMainPage();
//
//   });
//   
//
//
////*******SETTINGS PAGE BINDINGS ********
////**************************************
//   $('#settings').bind("pageinit", function()
//   {
//      var d = new Date();
//      var doy = d.getDOY();
//      var yearLen = d.getYearLen();
//      // Set cathcal version
//      $('#version').text($cc.version);
//
//      // Set daysbefore slider to prevent days outside the current year
//      $('#daysbefore').attr('min', 0);            
//      $('#daysbefore').attr('max', doy);     
//      $('#daysbefore').attr('value', config['daysbefore']);
//
//      // Set daysafter slider to prevent days outside the current year
//      $('#daysafter').attr('min', 0);   
//      $('#daysafter').attr('max', yearLen - doy);
//      $('#daysafter').attr('value', config['daysafter']);           
//
//      // Set the fixed Easter footer switch to the config value
//      //$('#fixedfooter')[0].selectedIndex = (config['fixedfooter']) ? 0:1;
//      //$('#fixedfooter').slider("refresh");
//
//   });
//
//   //*********** FUNCTIONS ****************
//   //**************************************
//   
//   // update the main page with
//   // the information in dayspec.
//   // dayspec is either an array of
//   // doy's or an integer specifying
//   // one day of the year. If dayspec
//   // is empty today is used as the
//   // day of the year.
//   function updateMainPage(dayspec,done)
//   {
//      showWorkingMsg();
//      $('#celeblisti li').remove();
//      if (typeof dayspec == 'undefined' || !dayspec)
//      {
//         // Get today's date and update list 
//         d = new Date();
//         var dayspec=d.getDOY();
//      }
//
//      populateList(dayspec,function(litdays)
//      {
//         // Insert values on header
//         var celeb=cathcal.getToday("%d - %c");
//
//         // Make today's feast the title & subtitle
//         $("#hceleb").text(celeb);
//
//         //Easter date in footer
//         $(".easterdate").text(cathcal.getEaster(null,"%s"));
//         hideWorkingMsg();
//         if (done)
//            done();
//      });
//   };
//
//   // Fill the <ul> element #celeblist with
//   // one <li> element for each liturgical day
//   // and call done() when finished. The 
//   // function works asynchronously.
//   function populateList(dayspec,done)
//   {
//      var i,beg,end,getDOY;
//
//      //$('#celeblist').hide(true);
//      $('#celeblist').html("");
//      
//      // dayspec is the doy on which the list is centered
//      // usually today.
//      if (typeof dayspec == 'number')
//      {
//         beg = 0 - config["daysbefore"];
//         end = config["daysafter"];
//         getDOY = function(i) { return dayspec + i; };
//      }
//      // dayspec is an array of doys to add to the list
//      else if (typeof dayspec == 'object' && dayspec.length > 0)
//      {
//         beg = 0;
//         end = dayspec.length;
//         getDOY = function(i) { return dayspec[i]; };
//      }
//
//      // Compute the feasts for
//      // a week and insert the 
//      // values on the main page
//
//      var litdays = [];
//      $cc.worker(beg, end, 8, 15, function (i)
//      {
//         litdays[i] = cathcal.getFeastForDOY(getDOY(i),"%O");
//         appendToList(i,litdays[i]);  // Append the list elements on the main page
//         
//      }, function () 
//      {
//         $('#celeblist').listview('refresh'); 
//         $('#celeblist').show();
//         done(litdays);
//      });
//   };
//
//   function cancelSettings()
//   {
//      //showOnly = true;
//      //alert("cancelSettins called " + showOnly);
//   };
//
//   function saveSettings()
//   {
//      // Update list with new number of days before and after today
//      config["daysbefore"] = $('#daysbefore').val();
//      config["daysafter"] =  $('#daysafter').val();
//
//      // Save footer positionsetting  according to fixedfooter slider switch
//      //var val =  $('#fixedfooter')[0].selectedIndex; 
//      //config["fixedfooter"] = (val == 0) ? true:false;
//
//      //$('#mainpage').trigger('pagecreate');
//      $('#mainpage').trigger('pageinit');
//      saveConfig();
//   };
//
//   // i is the day with 0=today, 1=tomorrow, -1=yesterday, etc.
//   function appendToList(i,litday)
//   {
//      var img = "";
//      var sw = "d"
//       
//      // Liturgical colors
//      var colors = litday.colors;
//
//      // Rank
//      var rank = litday.rank;
//
//      // Celebration
//      var celeb = litday.celebration;
//
//      // Date
//      var date = litday.date.toRelDateString("%M");
//      var doy = litday.date.getDOY();
//
//      // Liturgical Season
//      var season = litday.season.toString();
//
//      // Cycle of Mass readings
//      var cycle = litday.cycle;
//
//      var easter = $cc.getEaster();
//
//      // Use icon for today if necessary, and change swatch theme to 'b'
//      if ( i == 0 )
//      {
//         img = '<img src="images/today16x16.png" class="ui-li-icon"></img>';
//         sw = "b"
//      }
//
//      // Use golden swatch for a SOLEMNITY, and apropiate swatches
//      // for FEASTS and MEMORIALS
//      switch(rank)
//      {
//         case $cc.HOLYWEEK:
//         case $cc.TRIDUUM:
//            sw = "a";
//            break;
//         case $cc.SOLEMNITY:
//            sw = "e";
//            break;
//         case $cc.SUNDAY:
//            sw = "e";
//            break;
//         case $cc.LORD:
//         case $cc.FEAST:
//            sw = "e";
//            break;
//         case $cc.MEMORIAL:
//            if (i != 0) 
//               sw = "c";
//            break;
//         case $cc.OPTIONAL:
//         case $cc.COMMEMORATION:
//         case $cc.WEEKDAY:
//         case $cc.ASHWED:
//            if (i != 0) 
//               sw = "d";
//            break;
//      }
//
//      // Append one color bubble for each possible liturgical color
//      var k;
//      var bubbles = "";
//      if (arguments.length > 1)
//      {
//         bubbles = '<div class ="litcolor-table">';
//         for (k=0; k < colors.length; k++)
//         {
//            var c = colors[k];
//            bubbles += sprintf('<span id="lcolor-a-%d" class="litcolor litcolor-%s">%s</span>',i,c.toLowerCase(),c);
//         }
//         bubbles += '</div>'
//      }
//
//      // Append li item for day specified by i
//      $("#celeblist").append(' \
//            <li id="day-' + i + '" data-role="list-divider"> \
//            <span class="tooltipfixme">' + date + '<!--span class="classic">Type to search the calendar</span--></span> \
//            ' + bubbles + ' </li> \
//            <li data-theme="' + sw + '"> \
//            <a data-role="button" href="#" > \
//               ' + img + ' \
//               <h6 class="celeb">' + celeb + '</h6>\
//               <p class="ui-grid-b date-rank-color">\
//                  <span class="ui-block-a">' + season + '</span>\
//                  <span class="ui-block-c">' + cycle + '</span> \
//                  <span class="ui-block-b">' + rank + '</span> \
//               </p>  \
//            </a> \
//            </li> \
//         ');
//   };
//
//   // Search calendat year and update the main page
//   // with the results (stext is the search string)
//   function searchcal(stext)
//   {
//      if ( stext == "" || stext == null)
//         return;
//
//      showWorkingMsg("Searching...");
//      cathcal.search(stext.toLowerCase(), function(dayspec)
//      {
//         if (dayspec != null)
//         {
//            updateMainPage(dayspec, function ()
//            {
//               showMsg( dayspec.length + " result(s) found.");
//            });
//         }
//         else
//         {
//            hideWorkingMsg();
//            showMsg("No results found.");
//         }
//      });
//   };
//
//   function hideWorkingMsg()
//   {
//      //This is a bug in JQM which sometimes
//      // doesn't load remove the ui-loading class              
//      // and therefore the msg doesn't hide             
//      $('body').removeClass('ui-loading'); 
//                                        
//      $.mobile.hidePageLoadingMsg();
//   };
//
//   function showWorkingMsg(msg)
//   {
//      if (msg)
//         $.mobile.loadingMessage = msg;
//      else
//         $.mobile.loadingMessage = "Loading...";
//
//      //This is a bug in JQM which sometimes
//      // doesn't load the ui-loading class              
//      // and therefore the msg doesn't show             
//      $('body').addClass('ui-loading'); 
//                                        
//      $.mobile.showPageLoadingMsg();
//   };
//
//   function runTest(year)
//   {
//         if (!year) throw Error("runTest(): Invalid year: " + year);
//         $("#test").css("display", "block");
//         cathcal.test(year);
//   };
//
//   function showMsg(msg,delay)
//   {
//      $('body').append('<div class="ui-message"> <div style="display:table-cell; \
//                        vertical-align:middle" class="ui-center"> \
//                        <div > \
//                        ' + msg + '</div></div></div>');
//      if (delay == undefined)
//         delay = 3000;
//
//      setTimeout(function()
//      {
//         $('body .ui-message').remove();
//      }, delay);
//   };
//
//   // Save the configuration in cookies
//   function saveConfig()
//   {
//      for (name in config)
//      {
//         var val = config[name];
//         if (val != undefined)
//            $cc.makeCookie(name,config[name],160)
//      }
//   };
//
//   // Load the configuration from cookies if they
//   // exist, otherwise keep the default values
//   function loadConfig()
//   {
//      for (name in config)
//      {
//         var val = $cc.readCookie(name);
//         if (val)
//            config[name] = val;
//      }
//   };
