// Set to true to enable debug info printed to the console
_MEDAID_DEBUG=true;
_MEDAID_VERSION = 0.12

//########################################################################
//########################################################################
//------------------------- INITIALIZATION -------------------------------
//########################################################################
//########################################################################
//

// Medicine type constants
// Main object (will be abbreviated $ma) 
medaid = { 
           DAILY : "Daily",
           WEEKLY: "Weekly",
           ASNEEDED: "As needed",

           console: {  assert: function(){},  
                        log: function(){},  
                        warn: function(){},  
                        error: function(){},  
                        debug: function(){},  
                        dir: function(){},  
                        info: function(){}
                     },

            // The object contained in the array exists only for
            // documentation purposes so that the properties 
            // may be known easily.
				meds: [{ name: "Keppra",			   // Medicine name
							desc: "Levetiracetam",	   // Description
							type: "Daily",			      // Daily, Weekly, or As needed
							doses:[{
								time: new Date(2012,1,1,9,00),
								count: 3 
							},
							{ 
							  	time: new Date(2012,1,1,21,00),
								count: 3 
							}								// time/date for dose and 
															// number of doses
							],
							dosesLeft: 30,          // Pills or inhalations left
							dosesLeftDate: new Date(2012,1,1,9,0), // Date for doses left
							daysBeforeRunoutAlarm: 12, // Alarm on if runout date
		                                // is within this number of days
							onfile: true,              // True if prescription on file
		                                // at Mayo
							refillOrdered: true,       // True if refill has been
		                                // ordered
                     refillsLeft: 8,
                     pharmacy: "Mayo pharmacy",
                     pharmacyPhone: "(800) 445-6326",
                     rxNumber: 9783982,
							_dosesPerPeriod: 6,        // Total doses per period
							_runoutDate: new Date(2012,1,20,21,00),
							_refillDate: new Date(2012,2,22,21,00),
							_daysLeft: 20,             // Number of days left with
		                                // at current dosage
                     _daysUntilRefill: 20,
							_alarm: true               // Alarm to refill
                  }],

            //Set to true when all calculations are finished
            ready: false,
            version: _MEDAID_VERSION
};


// Abbreviated name
$ma=medaid;
$ma.tout = [];

//********************************************************************
// Purpose:       Calculate all the necessary values for all the 
//                medicines
// Parameters:    none
// Description:   This function calls updateMed() for each medicine
//******************************************************************** 
$ma.updateValues = function(callback)
{
   var i=0;
   for(i=0; i < $ma.meds.length; i++)
   {
      $ma.updateMed($ma.meds[i]);
   }
	callback();
}

//********************************************************************
// Purpose:       Calculate all the necessary values for the specified
//                medication
// Parameters:    the medication object to update 
// Description:   This function updates  _dosesPerPeriod,
//                _runoutDate, _refillDate, _daysLeft, and _alarm
//******************************************************************** 
$ma.updateMed = function(med)
{

	this.update_dosesPerPeriod(med);
	this.update_runoutDate(med);
	this.update_refillDate(med);
	//this.update_daysLeft(med);
	this.update_alarm(med);
}


//********************************************************************
// Purpose:       Calculate _dosesPerPeriod for the specified    
//                medicine
// Parameters:    Medicine to update
// Description:   This function adds the doses in the medicine
//                and calculates the total.
// Returns:       The number of doses needed per period
//                or null if there is no dosage information
//                (or medicine type is ASNEEDED)
//******************************************************************** 
$ma.update_dosesPerPeriod = function(med)
{
	// Add up the number of doses per
	// period
	var i,dose;
   sum=0;
	for(i=0; i<med.doses.length; i++)
	{
		dose = doses[i];
		sum = sum + dose.count;
	}

	// Set result to null if there is not 
	// enough information
   if ( med.doses.length <= 0 || sum === 0) 
		sum = null;
	med._dosesPerPeriod = sum;

	return med._dosesPerPeriod;
}

//********************************************************************
// Purpose:       Calculate _runoutDater for the specified    
//                medicine
// Parameters:    Medicine to update
// Description:   This function calculates the day when the medicine
//                will run out
// Returns:       The date when the medicine will run out or
//                null if daysLeft could not be calculated.
//******************************************************************** 
$ma.update_runoutDate = function(med)
{
	// runoutDate = TODAY() + _daysLeft
	//=IF(OR(H3=0,ISBLANK(E3)),"-",TODAY()+L3)

	var daysLeft = this.update_daysLeft(med);

	// Calculate runout date if daysLeft is available
	// otherwise return null
	if (daysLeft != null)
      med._runoutDate = daysLeft * 24 * 60 * 60 * 1000;
	else
		med._runoutDate = null;

	return med._runoutDate;
}

//********************************************************************
// Purpose:       Calculate _refillFate for the specified medicine    
//                
// Parameters:    Medicine to update
// Description:   This function calculates the day when the medicine
//                needs to be refilled. That is, the difference 
//                between runoutDate and daysBeforeRunoutAlarm
// Returns:       The refill date or null if the runoutDate
//                could not be calculated.
//******************************************************************** 
$ma.update_refillDate = function(med)
{
	//refillDate = runoutDate -  daysBeforeRunoutAlarm
	var runoutDate = this.update_runoutDate(med);

	// Calculate runout date if daysLeft is available
	// otherwise return null
	if (runoutDate != null)
      med._refillDate = runoutDate - daysBeforeRunoutAlarm * 24 * 60 * 60 * 1000;
	else
		med._refillDate = null;
}

//********************************************************************
// Purpose:       Calculates the _daysLeft for the specified medicine 
//                
// Parameters:    Medicine to update
// Description:   This function calculates the days left for a      
//                the medicine. It divides the doses left by   
//                the number of doses needed per period, converts
//                this number to days depending on the type of 
//                medicine (Weekly, Daily or AsNeeded) and substracts
//                this from the number of days that have passed
//                since we updated the doses left.
//
// Returns:       The days left for a medicine
//                could not be calculated.
//******************************************************************** 
$ma.update_daysLeft = function(med)
{
	// dosesLeft/dosesPerPeriod - DAYS( TODAY(),dosesLeftDate ) 
	var periodsLeft = med.dosesLeft/dosesPerPeriod;
	var daysPerPeriod=1;

	// Modify daysPerPeriod accorging to type of medicine
	switch(med.type)
	{
		case $ma.DAILY:
	      daysPerPeriod=1;
			break;
		case $ma.WEEKLY:
	      daysPerPeriod=7;
			break;
		case $ma.ASNEEDED:
	      daysPerPeriod=1;
			break;
	}
	// Milliseconds difference between today and dosesLeftDate
   var ms_diff = new Date() - med.dosesLeftDate;

	// Convert milliseconds to days
   var days_diff = ms_diff / 1000 / 60 / 60 / 24;

   //	Calculate days left of medicine
	//	a negative number means we're ahead that number of days 
	var daysLeft = periodsLeft * daysPerPeriod  - days_diff;
	med._daysLeft = daysLeft;

	return med._daysLeft;
}

$ma.update_alarm = function(med)
{
	// IF(daysLeft <= daysBeforeRunoutAlarm,"Yes","No")
   if (med._daysLeft <= daysBeforeRunoutAlarm)
	   med._alarm = true;
   else
	   med._alarm = false;	

	return med._alarm;
}

$ma.autoUpdate = function(seconds,callback)
{
	setInterval($ma.updateValues(callback), seconds * 1000);
}

$ma.tlog = function ()
{
   var i;

   for(i=0; i < arguments.length; i++)
   {
      $ma.tout.push("<li>" + arguments[i]);
   }

   $('#medaid-test').html($ma.tout.join(""));
}

// Setup console methods to empty if there is no console object
// or if _MEDAID_DEBUG is not set
if (typeof console == "undefined")
   console=medaid.console; 
if (!_MEDAID_DEBUG)
   console.debug = function(args) {};

try 
{
   $ma.config = 
   {
      daysbefore: 0,
      daysafter: 35,
      showeaster: true
   };

   //loadConfig();

   //******** DOCUMENT  BINDINGS ********
   //************************************
   var h1Collapsibles;

   $ma.tlog("Javascript init function.");
   Zepto(function($)
   {
      $ma.tlog("Zepto initialization function.");
      //Show only the main page at first
      $('[data-role="page"]').hide().first().show();

      //Store collapsibles zepto array to use later
      h1Collapsibles = $('div[data-role="collapsible"] h1');

      //Hide section data in the beginning
      hideSection(h1Collapsibles);

      //Toggle collapsible section on click
      h1Collapsibles.on("click",function(e)
      {
         toggleSection($(this));
      })
   });

   // Hide section(s) indicated by h1el
   function hideSection(h1el)
   {
      // If caller passes a collection hide all
      // sections in the collection
      if ( $.isArray(h1el) )
         h1el.each(function(index)
         {
            hideSection(h1el);
         })

      // Hide the div after the h1 element
      // the div has the contents of that section
      // whose title is the h1 element.
      var el = h1el.next();
      if (el.is('div'))
         el.hide();
      else
         $ma.tlog('div needs to be after h1 in collapsible ' + h1el);
   }

   function toggleSection(h1el)
   {
      //Toggle the specified section
      var el = h1el.next()
      if (el.is('div'))
      {
         $ma.tlog("Toggling " + h1Collapsibles.index(h1el))
         el.toggle()
      }
      else
         $ma.tlog('div needs to be after h1 in collapsible ' + h1el)
      
      //Hide the other sections
      h1Collapsibles.each(function(index)
      {
         if ( index != h1Collapsibles.index(h1el))
            hideSection($(this))
      })
   }
}
catch(e)
{
   $ma.tlog("Exception loading medaid-ui.js: ",e);
}

//******** MAIN PAGE BINDINGS ********
//************************************
   // Hide blackberry's URL bar
//   $('#mainpage').bind("vmousemove", function(e)
//   {
//      var entered;
//      if (!entered && e.pageY <= 59) 
//      {
//         entered =true;
//         if (typeof blackberry != 'undefined')
//            $.mobile.silentScroll(59);
//      }
//      else
//      {
//         entered=false;
//      }
//   });
//
//   $('#mainpage').bind("pageinit", function()
//   {
//      // Page has now been enhanced by jQuery mobile
//
//
//      // JQuery mobile prevents links to 
//      // local anchors from working. The
//      // following code allows it for all 
//      // elements with attribute data-linkanchor 
//      // set to trueclass.
//      $('[data-linkanchor="true"]').bind('click', function(e) 
//      {
//         var offset = $( $(this).attr('href') ).get(0).offsetTop;
//         $.mobile.silentScroll(offset);
//         return false;
//      });
//
//      //********  SEARCH BAR  BINDINGS ********
//
//      // Start search if the search bar loses focus
//      $('#search-cal').change(function(e)
//      {
//         // Update main page if we have some results
//         // otherwise leave it alone
//         var stext = $('#search-cal').val();
//         searchcal(stext);
//      });
//
//      
//      //******** INITIALIZE LIST **************
//      //******** WITH TODAY AS   **************
//      //******** CENTRAL DAY     **************
//
//      // Hide the URL bar in blackberry
//      // bold 9700 (screen res. 480x360) 
//      if (typeof blackberry != 'undefined')
//         $.mobile.silentScroll(59);
//
//      updateMainPage();
//
//   });
//   
//
//
////*******SETTINGS PAGE BINDINGS ********
////**************************************
//   $('#settings').bind("pageinit", function()
//   {
//      var d = new Date();
//      var doy = d.getDOY();
//      var yearLen = d.getYearLen();
//      // Set cathcal version
//      $('#version').text($cc.version);
//
//      // Set daysbefore slider to prevent days outside the current year
//      $('#daysbefore').attr('min', 0);            
//      $('#daysbefore').attr('max', doy);     
//      $('#daysbefore').attr('value', config['daysbefore']);
//
//      // Set daysafter slider to prevent days outside the current year
//      $('#daysafter').attr('min', 0);   
//      $('#daysafter').attr('max', yearLen - doy);
//      $('#daysafter').attr('value', config['daysafter']);           
//
//      // Set the fixed Easter footer switch to the config value
//      //$('#fixedfooter')[0].selectedIndex = (config['fixedfooter']) ? 0:1;
//      //$('#fixedfooter').slider("refresh");
//
//   });
//
//   //*********** FUNCTIONS ****************
//   //**************************************
//   
//   // update the main page with
//   // the information in dayspec.
//   // dayspec is either an array of
//   // doy's or an integer specifying
//   // one day of the year. If dayspec
//   // is empty today is used as the
//   // day of the year.
//   function updateMainPage(dayspec,done)
//   {
//      showWorkingMsg();
//      $('#celeblisti li').remove();
//      if (typeof dayspec == 'undefined' || !dayspec)
//      {
//         // Get today's date and update list 
//         d = new Date();
//         var dayspec=d.getDOY();
//      }
//
//      populateList(dayspec,function(litdays)
//      {
//         // Insert values on header
//         var celeb=cathcal.getToday("%d - %c");
//
//         // Make today's feast the title & subtitle
//         $("#hceleb").text(celeb);
//
//         //Easter date in footer
//         $(".easterdate").text(cathcal.getEaster(null,"%s"));
//         hideWorkingMsg();
//         if (done)
//            done();
//      });
//   };
//
//   // Fill the <ul> element #celeblist with
//   // one <li> element for each liturgical day
//   // and call done() when finished. The 
//   // function works asynchronously.
//   function populateList(dayspec,done)
//   {
//      var i,beg,end,getDOY;
//
//      //$('#celeblist').hide(true);
//      $('#celeblist').html("");
//      
//      // dayspec is the doy on which the list is centered
//      // usually today.
//      if (typeof dayspec == 'number')
//      {
//         beg = 0 - config["daysbefore"];
//         end = config["daysafter"];
//         getDOY = function(i) { return dayspec + i; };
//      }
//      // dayspec is an array of doys to add to the list
//      else if (typeof dayspec == 'object' && dayspec.length > 0)
//      {
//         beg = 0;
//         end = dayspec.length;
//         getDOY = function(i) { return dayspec[i]; };
//      }
//
//      // Compute the feasts for
//      // a week and insert the 
//      // values on the main page
//
//      var litdays = [];
//      $cc.worker(beg, end, 8, 15, function (i)
//      {
//         litdays[i] = cathcal.getFeastForDOY(getDOY(i),"%O");
//         appendToList(i,litdays[i]);  // Append the list elements on the main page
//         
//      }, function () 
//      {
//         $('#celeblist').listview('refresh'); 
//         $('#celeblist').show();
//         done(litdays);
//      });
//   };
//
//   function cancelSettings()
//   {
//      //showOnly = true;
//      //alert("cancelSettins called " + showOnly);
//   };
//
//   function saveSettings()
//   {
//      // Update list with new number of days before and after today
//      config["daysbefore"] = $('#daysbefore').val();
//      config["daysafter"] =  $('#daysafter').val();
//
//      // Save footer positionsetting  according to fixedfooter slider switch
//      //var val =  $('#fixedfooter')[0].selectedIndex; 
//      //config["fixedfooter"] = (val == 0) ? true:false;
//
//      //$('#mainpage').trigger('pagecreate');
//      $('#mainpage').trigger('pageinit');
//      saveConfig();
//   };
//
//   // i is the day with 0=today, 1=tomorrow, -1=yesterday, etc.
//   function appendToList(i,litday)
//   {
//      var img = "";
//      var sw = "d"
//       
//      // Liturgical colors
//      var colors = litday.colors;
//
//      // Rank
//      var rank = litday.rank;
//
//      // Celebration
//      var celeb = litday.celebration;
//
//      // Date
//      var date = litday.date.toRelDateString("%M");
//      var doy = litday.date.getDOY();
//
//      // Liturgical Season
//      var season = litday.season.toString();
//
//      // Cycle of Mass readings
//      var cycle = litday.cycle;
//
//      var easter = $cc.getEaster();
//
//      // Use icon for today if necessary, and change swatch theme to 'b'
//      if ( i == 0 )
//      {
//         img = '<img src="images/today16x16.png" class="ui-li-icon"></img>';
//         sw = "b"
//      }
//
//      // Use golden swatch for a SOLEMNITY, and apropiate swatches
//      // for FEASTS and MEMORIALS
//      switch(rank)
//      {
//         case $cc.HOLYWEEK:
//         case $cc.TRIDUUM:
//            sw = "a";
//            break;
//         case $cc.SOLEMNITY:
//            sw = "e";
//            break;
//         case $cc.SUNDAY:
//            sw = "e";
//            break;
//         case $cc.LORD:
//         case $cc.FEAST:
//            sw = "e";
//            break;
//         case $cc.MEMORIAL:
//            if (i != 0) 
//               sw = "c";
//            break;
//         case $cc.OPTIONAL:
//         case $cc.COMMEMORATION:
//         case $cc.WEEKDAY:
//         case $cc.ASHWED:
//            if (i != 0) 
//               sw = "d";
//            break;
//      }
//
//      // Append one color bubble for each possible liturgical color
//      var k;
//      var bubbles = "";
//      if (arguments.length > 1)
//      {
//         bubbles = '<div class ="litcolor-table">';
//         for (k=0; k < colors.length; k++)
//         {
//            var c = colors[k];
//            bubbles += sprintf('<span id="lcolor-a-%d" class="litcolor litcolor-%s">%s</span>',i,c.toLowerCase(),c);
//         }
//         bubbles += '</div>'
//      }
//
//      // Append li item for day specified by i
//      $("#celeblist").append(' \
//            <li id="day-' + i + '" data-role="list-divider"> \
//            <span class="tooltipfixme">' + date + '<!--span class="classic">Type to search the calendar</span--></span> \
//            ' + bubbles + ' </li> \
//            <li data-theme="' + sw + '"> \
//            <a data-role="button" href="#" > \
//               ' + img + ' \
//               <h6 class="celeb">' + celeb + '</h6>\
//               <p class="ui-grid-b date-rank-color">\
//                  <span class="ui-block-a">' + season + '</span>\
//                  <span class="ui-block-c">' + cycle + '</span> \
//                  <span class="ui-block-b">' + rank + '</span> \
//               </p>  \
//            </a> \
//            </li> \
//         ');
//   };
//
//   // Search calendat year and update the main page
//   // with the results (stext is the search string)
//   function searchcal(stext)
//   {
//      if ( stext == "" || stext == null)
//         return;
//
//      showWorkingMsg("Searching...");
//      cathcal.search(stext.toLowerCase(), function(dayspec)
//      {
//         if (dayspec != null)
//         {
//            updateMainPage(dayspec, function ()
//            {
//               showMsg( dayspec.length + " result(s) found.");
//            });
//         }
//         else
//         {
//            hideWorkingMsg();
//            showMsg("No results found.");
//         }
//      });
//   };
//
//   function hideWorkingMsg()
//   {
//      //This is a bug in JQM which sometimes
//      // doesn't load remove the ui-loading class              
//      // and therefore the msg doesn't hide             
//      $('body').removeClass('ui-loading'); 
//                                        
//      $.mobile.hidePageLoadingMsg();
//   };
//
//   function showWorkingMsg(msg)
//   {
//      if (msg)
//         $.mobile.loadingMessage = msg;
//      else
//         $.mobile.loadingMessage = "Loading...";
//
//      //This is a bug in JQM which sometimes
//      // doesn't load the ui-loading class              
//      // and therefore the msg doesn't show             
//      $('body').addClass('ui-loading'); 
//                                        
//      $.mobile.showPageLoadingMsg();
//   };
//
//   function runTest(year)
//   {
//         if (!year) throw Error("runTest(): Invalid year: " + year);
//         $("#test").css("display", "block");
//         cathcal.test(year);
//   };
//
//   function showMsg(msg,delay)
//   {
//      $('body').append('<div class="ui-message"> <div style="display:table-cell; \
//                        vertical-align:middle" class="ui-center"> \
//                        <div > \
//                        ' + msg + '</div></div></div>');
//      if (delay == undefined)
//         delay = 3000;
//
//      setTimeout(function()
//      {
//         $('body .ui-message').remove();
//      }, delay);
//   };
//
//   // Save the configuration in cookies
//   function saveConfig()
//   {
//      for (name in config)
//      {
//         var val = config[name];
//         if (val != undefined)
//            $cc.makeCookie(name,config[name],160)
//      }
//   };
//
//   // Load the configuration from cookies if they
//   // exist, otherwise keep the default values
//   function loadConfig()
//   {
//      for (name in config)
//      {
//         var val = $cc.readCookie(name);
//         if (val)
//            config[name] = val;
//      }
//   };
