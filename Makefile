PROD=no
DIST=dist
PRJ=medaid



VERSIONFILE=$(PRJ)-version
VERSION:=$(shell tail -1 $(VERSIONFILE))

WWWOUT=$(DIST)/www
BASEURL:=ftp://ftp.newsfromgod.com/public_html/newsfromgod
UPWWWURL=$(BASEURL)/$(PRJ)/

SRC= .htaccess           \
	  tools/srest.min.js  \
	  tools/fx_methods.js \
	  Manifest			    \
	  medaid.css          \
	  medaid.html

JSSRC= medaid.js         \
	    medaid-ui.js

#JSTEST= test/medaid.test.js

JSOUT= $(WWWOUT)/$(PRJ).js

#Build distribution, copying needed files to $WWWOUT,
#and updating everything
.PHONY: dist
dist: $(SRC) $(JSSRC) $(JSOUT) $(JSTEST)
	cp $(SRC) $(WWWOUT)

#Build main project javascript, concatenating all js into one file
$(JSOUT): $(JSSRC) $(JSTEST)
	@[[ -d $(WWWOUT) ]] || mkdir -p $(WWWOUT)
	@if [[ "$(PROD)" -eq "no" ]]; then          \
		echo "Building development version.";    \
	   echo cat $(JSSRC) $(JSTEST) \> $(JSOUT); \
	   cat $(JSSRC) $(JSTEST) > $(JSOUT);       \
	else                           			     \
		echo "Building production version.";     \
	   echo cat $(JSSRC) \> $(JSOUT);    	     \
	   cat $(JSSRC) > $(JSOUT);    			     \
	fi 

#Update version number in srest.cgi
$(PRJ).cgi: $(VERSIONFILE)
	sed -i 's/^\s*\$$_MEDAID_VERSION\s*=.*/$$_MEDAID_VERSION = $(VERSION);/' $@

#Update version number in srest.js
$(PRJ).js: $(VERSIONFILE)
	sed -i "s/^\s*\_MEDAID_VERSION\s*=.*/_MEDAID_VERSION = $(VERSION)/" $@

#These files don't need any specifig rule to be built
#this is mainly so Make doesn't complain about not having rules
test/medaid-test.js:
tools/zepto.min.js:
srest.htm:

#upload files to server
upload: dist
	@while [[ -z "$$UPUSER" ]]; do \
		read -er -p "Ftp upload server User: " UPUSER;\
	done && \
	while [ -z "$$UPPWD" ]; do \
		read -er -p "Ftp upload server password: " UPPWD; \
	done && shopt -s dotglob && \
	   cd "$(WWWOUT)" && \
		   FILES="`ls -m * | sed -e 's/, /,/g' | tr -d '\n'`" && \
			printf "\nUploading $$FILES:\n" && \
	      curl -T \{"$$FILES"\} $(UPWWWURL) -u "$$UPUSER:$$UPPWD";

clean:
	rm -rf "$(WWWOUT)/"

